National Ambulatory Medical Care Survey (NAMCS) Dataset
=======================================================

This package contains data on statin use and health outcomes from the
National Ambulatory Medical Care Survey (NAMCS).

Installation
============

You can install the package from Bitbucket.

    library("devtools")
    devtools::install_bitbucket("novisci/statinr")

Codebook
========

-   patcode = patient code (character string)
-   newuser = new statin user (0=not a new user, 1=new user)
-   newusercat = new user, stratifying potency (0=not a new user, 1=low
    potency statin, 2=high potency statin)
-   t = observed time, in years (0-10)
-   delta = outcome status (0=admin censored at 10 years,
    1=hospitalization for CVD, 2=all-cause mortality, 3=loss
    to follow-up)
-   diabetes = current diabetes status (0=does not have diabetes,
    1=has diabetes)
-   htn = current hypertension status (0=does not have hypertension,
    1=has hypertension)
-   hyplipid = current hyperlipidemia status (0=does not have
    hyperlipidemia, 1=has hyperlipidemia)
-   white = race (0=black/other, 1=white)
-   year = calendar year (2000-2014)
-   male = sex (0=female, 1=male)
-   obese = obesity status (0=not obese, 1=obese)
-   age = patient age, in years (35-100)
-   smoke = current tobacco smoking status (0=not a current smoker,
    1=current smoker)
-   dbp = diastolic blood pressure (continuous)
-   sbp = systolic blood pressure (continuous)
